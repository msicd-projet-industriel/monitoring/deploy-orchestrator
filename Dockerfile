FROM python:3.8-alpine

COPY . .

RUN apk update &&\
    apk add --no-cache openssh-client gettext gcc build-base libffi-dev curl &&\
    pip install --upgrade pip &&\
    pip install wheel --no-cache-dir &&\
    pip install ansible ansible-core ansible-lint yamllint PyYAML --no-cache-dir

WORKDIR /root

CMD [ "/bin/sh" ]
